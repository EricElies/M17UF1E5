using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameObject player;
    
    
    

    public bool facingLeft = false;

   

    private void Start()
    {
        player = FindObjectOfType<PlayerController>().gameObject;
        
        //enemyController = FindObjectOfType<EnemyController>().gameObject;



    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject != null)
        {
            //anim.Play("enemyAnimation");
            
            /*if (transform.position.x > player.transform.position.x && facingLeft == false)
            {
                transform.Rotate(0, 180, 0);
                facingLeft = true;
            }
            else if (transform.position.x < player.transform.position.x && facingLeft == true)
            {
                transform.Rotate(0, 180, 0);
                facingLeft = false;
            }
            */

            if (player.transform.position.y >= transform.position.y-1 )
            {
                Vector3 vector = new Vector3 (player.transform.position.x, transform.position.y, 0);
                transform.position = Vector3.MoveTowards(transform.position, vector, 0.7f * Time.deltaTime);
            }
        }
    }

    


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.name.Equals("Sword(Clone)")) 
        {
            StartCoroutine(deadEffectEnemy()); 
        }
        
        

    }

    IEnumerator deadEffectEnemy()
    {
        
        GetComponent<SpriteRenderer>().color = Color.red;
        yield return new WaitForSeconds(.3f);

        Destroy(gameObject);
    }
}