using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    private float speed = 5f;
    private float jumpForce = 6f;
    private float moveInput;

    private Rigidbody2D rb;

    private bool isGrounded;
    public Transform groundCheck, firePoint;
    public float checkRadius;
    public LayerMask whatIsGround;
    public int extraJump;
    public Vector3 initPos;
    public GameObject player, bullet;
    private Animator anim;
    public CameraFollow cam;

    



    public static object Instance { get; internal set; }

    private void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        initPos = transform.position;
        
        


    }

    private void FixedUpdate()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);

        moveInput = Input.GetAxis("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);
    }

    private void Update()
    {
        jump();
        shoot();
        animatorControl();
        

        
    }

    public void ResetPosition() {
        
        cam.maxPosY = initPos.y;
        this.transform.position = initPos;
    }


    public void jump() {
        
        if (Input.GetKeyDown(KeyCode.Space)){
           
            if (extraJump != 0)
                {
                    isGrounded = false;
                    rb.velocity = Vector2.up * jumpForce;
                    extraJump--;
                
            } 
            }

        if (isGrounded == true) {
                extraJump = 1;  
        }
    }

    public void animatorControl() 
    {
        if (isGrounded == true) 
        {
            anim.SetBool("Jump", false);
            anim.SetBool("Land", false);
        }
        if (isGrounded == false && rb.velocity.y > 0f) 
        {
            anim.SetBool("Jump", true);
            
            anim.SetBool("Land", false);
        }
        if (isGrounded == false && rb.velocity.y < 0)
        {
            anim.SetBool("Jump", false);
            anim.SetBool("Land", true);
        }
    }
    
   

    public void shoot() 
    {
        if (Input.GetKeyDown(KeyCode.LeftControl)) 
        {
            
            Instantiate(bullet, firePoint);
            
        }
    }

    


}