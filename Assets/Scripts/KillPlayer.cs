using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class KillPlayer : MonoBehaviour
{
    
    public PlayerController playerController;
    public LevelManager lv;
    public Score score;
    public int lifes = 2;
    
    
    private void Start()
    {
        playerController = FindObjectOfType<PlayerController>();
        
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (lifes >= 0)
        {
            if (collision.gameObject.CompareTag("Enemy"))
            {
                Destroy(collision.gameObject);
                StartCoroutine(receiveDmg());
                
            }
        }
       
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (lifes >= 0)
        {   
            if (lifes <= 2)
            {
                if (collision.gameObject.CompareTag("HEART"))
                {
                    lifes++;
                    Destroy(collision.gameObject);
                }
            }
            if (collision.gameObject.CompareTag("DeathZone")) {
                StartCoroutine(receiveDmg());
                
            }
        }
        
    }


    IEnumerator receiveDmg() 
    {   
        playerController.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
        playerController.GetComponent<SpriteRenderer>().color = Color.red;         
        yield return new WaitForSeconds(1f);
        lifes--;
        playerController.GetComponent<SpriteRenderer>().color = Color.white;
        playerController.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
        playerController.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        playerController.ResetPosition();
    }

    private void Update()
    {
        if (lifes < 0) {
            score.maxScore = lv.distancetext.text;
            SceneManager.LoadScene("GameOver"); 
        }
    }
}
