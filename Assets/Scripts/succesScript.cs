using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class succesScript : MonoBehaviour
{
    public LevelManager lv;
    public Score score;

    private void Start()
    {
        lv = FindObjectOfType<LevelManager>().GetComponent<LevelManager>();
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        score.maxScore = lv.distancetext.text;
        SceneManager.LoadScene("Succes");
        
    }
}
