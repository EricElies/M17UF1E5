using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bullet : MonoBehaviour
{
    Rigidbody2D rb;
    Transform player;
    float iniPosition;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = FindObjectOfType<PlayerController>().transform;
        iniPosition = player.position.x;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (iniPosition > 0)
        {
            rb.velocity = new Vector2(-5f, 0);
        }
        else
        {
            rb.velocity = new Vector2(5f, 0);
            transform.localScale = new Vector3(-1f , 1f , 1f);
        }
    }
}
