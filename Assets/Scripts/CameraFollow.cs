using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject follow;
    public Vector2 minCamPos, maxCamPos ;
    public LevelGenerator levelGenerator;
    public float maxPosY;
    
    


    // Start is called before the first frame update
    void Start()
    {
        float posX = follow.transform.position.x;
        float posY = follow.transform.position.y;
        

        camControl(posX, posY);

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float posX = follow.transform.position.x;
        float posY = follow.transform.position.y;
        maxCamPos.y = levelGenerator.lastPositionPlatform.y;
        
        // Use this condition to freeze cam position when player falls
        if(maxPosY <= posY)
        camControl(posX, posY);

    }


    public void camControl(float posX, float posY) 
    {
        maxPosY = posY;
        transform.position = new Vector3(Mathf.Clamp(posX, minCamPos.x, maxCamPos.x),Mathf.Clamp(posY, minCamPos.y, maxCamPos.y),transform.position.z);
    }
    

    
}
