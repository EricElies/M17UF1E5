using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGenerator : MonoBehaviour
{
    public GameObject[] platforms;
    public GameObject lastPlatform, healPlatform;

    public Vector3 positionSpawnPlatform;
    public Vector3 lastPositionPlatform;

    // Start is called before the first frame update
    void Start()
    {

        positionSpawnPlatform.z = 0;

        for (int i = 0; i < 20; i++)
        {
            
            if (i == 19)
            {
                RandomPosition(i);
                Instantiate(lastPlatform, positionSpawnPlatform, Quaternion.identity);
                lastPositionPlatform = positionSpawnPlatform;
            }
            else if (i % 10 == 0) {
                RandomPosition(i);
                Instantiate(healPlatform, positionSpawnPlatform, Quaternion.identity);
                lastPositionPlatform = positionSpawnPlatform;
            }
            else
            {
                RandomPosition(i);
                int randomPlataform = (int)Random.Range(0f, platforms.Length);
                Instantiate(platforms[randomPlataform], positionSpawnPlatform, Quaternion.identity);
                lastPositionPlatform = positionSpawnPlatform;
            }
        }
        
    }

    void RandomPosition(int i)
    {
        positionSpawnPlatform.x = Random.Range(-2f, 2f);
        positionSpawnPlatform.y = 3 * i;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
