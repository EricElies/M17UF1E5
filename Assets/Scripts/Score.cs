using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Score", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]
public class Score : ScriptableObject
{
    public string maxScore;
    
}
