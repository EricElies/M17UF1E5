using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;
    public static LevelManager Instance { get { return _instance; } }

    
    
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }


    public Text healthtext;
    public Text distancetext;
    public KillPlayer killPlayer;
    public PlayerController player;
    private float maxDistance;

    private void Start()
    {
        distancetext.text = "Distance: " + 0;

        
    }


    void Update()
    {
        
        healthtext.text = "Hearts: " + killPlayer.GetComponent<KillPlayer>().lifes;
        if (maxDistance < 0)
        {
            distancetext.text = "Distance: " + 0;
        }
        else if (maxDistance < player.GetComponent<PlayerController>().transform.position.y)
        {
            maxDistance = player.GetComponent<PlayerController>().transform.position.y;
            
        }
        
        
        distancetext.text = "Distance:  " + maxDistance;
    }
}
