using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdviceScript : MonoBehaviour
{

    public GameObject adviceJump, adviceShoot;
    
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("AdviceJump"))
        {
            adviceJump.SetActive(true);
        }
        if (collision.CompareTag("AdviceShoot"))
        {
            adviceShoot.SetActive(true);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("AdviceJump"))
        {
            adviceJump.SetActive(false);
        }
        if (collision.CompareTag("AdviceShoot"))
        {
            adviceShoot.SetActive(false);
        }

    }
    
}
